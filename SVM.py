import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets, metrics  # pip install scikit-learn
from sklearn.model_selection import train_test_split
# from mlxtend.plotting import plot_decision_regions

# Import dataset
iris = datasets.load_iris()

x = iris.data  # Load in data from the iris dataset
# y = iris.target.reshape(-1, 1)  # Load in target labels from the iris dataset
y = iris.target  # Load in target labels from the iris dataset

# Split up the data into training and testing datasets
x_train, x_test, y_train, y_test = train_test_split( x, y, test_size = 0.4, random_state = 100)
# x_train, x_test, y_train, y_test = train_test_split( x[:,2:], y, test_size = 0.5, random_state = 100)

# Create SVM Classifier, fit to the training data, then predict using the testing data.
clf = svm.SVC(kernel='linear')
clf.fit(x_train, y_train)  # Train the model using the training sets
y_pred = clf.predict(x_test)  # Predict the response for test dataset
svm_score = metrics.accuracy_score(y_test, y_pred)  # Accuracy percentage

# # Plot Decision Region using mlxtend
# plot_decision_regions(X=x[:,2:], y=y, clf=clf, legend=2)
# plt.show()
