from sklearn import datasets, tree, metrics, multioutput, ensemble
from sklearn.model_selection import train_test_split

# Load the digits dataset
digits = datasets.load_digits()

x = digits.data  # Load in data from the digits dataset
y = digits.target.reshape(-1, 1)  # Load in target labels from the digits dataset

# Split up the data into training and testing datasets
x_train, x_test, y_train, y_test = train_test_split( x, y, test_size = 0.2, random_state = 100)

# Create Decision Tree Classifier, fit to the training data, then predict using the testing data.
d_tree = tree.DecisionTreeClassifier()
d_tree.fit(x_train,y_train)
y_predict = d_tree.predict(x_test).reshape(-1,1)
results = metrics.confusion_matrix(y_test, y_predict)

tree_score = metrics.precision_score(y_test, y_predict, average = 'micro' )
